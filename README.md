# silhouette_score

### Ссылка на ноутбук в Google Colab:
https://colab.research.google.com/drive/1_B4e_gFQWZJ4iR3kucfIODzqkYlnGUkz?usp=sharing 

### Идея проекта:
Реализация метрики проверки качества кластеризации - Silouette_Score с помощью фреймворка ApacheSpark.

### Для проверки качества реализованной метрики выбраны синтетические датасеты:
* 'blobs.csv' - make_blobs(n_samples=100, centers=5, n_features=4, random_state=42)
* 'blobs_dev.csv' - make_blobs(n_samples=100, n_features=4, random_state=42, cluster_std=3.0)
* 'aniso.csv' - <p> X, y = make_blobs(n_samples=100, random_state=170) <br>
                transformation = [[0.6, -0.6], [-0.4, 0.8]] <br>
                X_aniso = np.dot(X, transformation) <br> </p>
* 'aniso_2.csv' - <p> X, y = make_blobs(n_samples=100, n_features=3, random_state=42) <br>
                transformation = [[0.4, -0.7, 0.4], [0.3, -0.9, 0.9], [0.3,-0.4, -0.9]] <br>
                X_aniso = np.dot(X, transformation) <br> </p>
* 'noisy_circles.csv' - make_circles(n_samples=100, factor=0.5, noise=0.05)
* 'varied.csv' - make_blobs(n_samples=100, cluster_std=[1.0, 2.5, 0.5], random_state=170)
* 'varied_2.csv' - make_blobs(n_samples=100, cluster_std=[1.0, 2.5, 0.5], random_state=99)


### Полученные результаты:
| Датасет | Реализованная (euclidean distance) | sklearn.metrics.silhouette_score |
| :---: | :---: | :---: | 
| 'blobs.csv' | 0.81743 | 0.80782 | 
| 'blobs_dev.csv' | 0.52434 | 0.50972 | 
| 'aniso.csv' | 0.45130 | 0.43506 | 
| 'aniso_2.csv' | 0.69984 | 0.69064 | 
| 'noisy_circles.csv' | 0.12137 | 0.10701 | 
| 'varied.csv' | 0.61220 | 0.60146 | 
| 'varied_2.csv' | 0.57320 | 0.56058 |

| Датасет | Реализованная (squaredEuclidean distance) | pyspark.ml.evaluation.ClusteringEvaluator |
| :---: | :---: | :---: | 
| 'blobs.csv' | 0.96028 | 0.95819 |
| 'blobs_dev.csv' | 0.73436 | 0.72642 |
| 'aniso.csv' | 0.53498 | 0.52272 |
| 'aniso_2.csv' | 0.86971 | 0.86585 |
| 'noisy_circles.csv' | 0.11596 | 0.10579 |
| 'varied.csv' | 0.69838 | 0.69209 |
| 'varied_2.csv' | 0.73825 | 0.731001 |

