from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from scipy.spatial.distance import pdist, squareform
from pyspark.sql.functions import monotonically_increasing_id 
from pyspark.sql.window import Window
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.evaluation import ClusteringEvaluator
from sklearn.metrics import silhouette_score
import pandas as pd

spark = SparkSession.builder.appName('spark').getOrCreate()
df = spark.read.options(header='True', inferSchema='True').format("csv").load("blobs.csv")
df = df.withColumnRenamed("_c0","idx")
df.cache()
df.printSchema()

feature_names = []
for i in df.columns:
  if i not in ['idx', 'y']:
    feature_names.append(i)

def silhouette(df):

  """Создаем квадратную матрицу попарных расстояний между точками датасета"""
  distM = spark.createDataFrame(pd.DataFrame(squareform( \ 
    pdist(df.select(feature_names).collect(), metric='euclidean')))) # metric='sqeuclidean'

  """Добавляем колонки индексов и лейблов точек"""
  distM = distM.withColumn('idx', f.row_number().over(Window.orderBy(monotonically_increasing_id()))-1) \
      .join(df.select('idx', 'y'), 'idx', 'left')

  """
  a_func - функция, которая рассчитывает среднее расстояние до точек внутри кластера, к которому сама точка принадлежит
  b_func - функция, которая рассчитывает среднее расстояние до точек другого ближайшего кластера
  """
  def a_func(k):
    return distM.groupBy('y').count().alias('c').join(distM.groupBy('y').sum(str(k)).alias('s'), 'y') \
      .withColumn('norm', f.col('s.sum('+str(k)+')')/(f.col('c.count')-1)) \
      .where(f.col('y') == (df.where(f.col('idx') == k).first()['y'])).first()['norm']

  def b_func(k):
    return distM.groupBy('y').count().alias('c').join(distM.groupBy('y').sum(str(k)).alias('s'), 'y') \
      .withColumn('norm', f.col('s.sum('+str(k)+')')/(f.col('c.count')-1)) \
      .where(f.col('y') != (df.where(f.col('idx') == k).first()['y'])).agg({'norm':'min'}).first()['min(norm)']

  n_samples = df.count()

  a, b = [], []
  for i in range(n_samples):
    a.append(a_func(i))
    b.append(b_func(i))

  ab = spark.createDataFrame(zip(a, b), schema=['a', 'b']) \
    .withColumn("idx", f.row_number().over(Window.orderBy(monotonically_increasing_id()))-1)

  """Внутри колонки 'sil' рассчитана метрика Ситуэта для каждой точки отдельно"""
  df = df.join(ab, 'idx') \
    .withColumn('sil', (f.col('b') - f.col('a')) / f.greatest(f.col('b'), f.col('a')))


  """sil_score - усредненное значение метрики Силуэта для всего датасета, именно её будем сравнивать с остальными реализациями."""
  sil_score = df.agg({'sil':'mean'}).first()[0]
  return sil_score

sil_score = silhouette(df)
print('Значение реализованной метрики Силуэт:', sil_score)

assembler = VectorAssembler(
    inputCols=[feature_names],
    outputCol="features")
feat = assembler.transform(df).select('features', 'y')

"""
silhouette - значение метрики Силуэта, реализованного в pyspark.
"""
evaluator = ClusteringEvaluator()
evaluator.setPredictionCol('y')
silhouette = evaluator.evaluate(feat)
print('Значение метрики Силуэта, реализованного в pyspark:', silhouette)

"""
silhouette_score - значение метрики Силуэта, реализованного в sklearn.
"""
silhouette_score = silhouette_score(df.toPandas()[feature_names], df.toPandas()['y'])
print('Значение метрики Силуэта, реализованного в sklearn:', silhouette_score)
